import math
import numpy as np
#import matplotlib.pyplot as plt

gov_first_unhappy={"demo":14,"rep":13,"mon":11}
gov_second_unhappy={"demo":16,"rep":14,"mon":12}

def gov_unhappy(gov,ncities):
    if (ncities<gov_first_unhappy[gov]): return 0
    else: return min(4,math.ceil((ncities-gov_first_unhappy[gov])/gov_second_unhappy[gov]))
    #else: return (ncities,math.ceil((ncities-gov_first_unhappy[gov])/gov_second_unhappy[gov]))

#print(gov_unhappy("demo",14))
#print(gov_unhappy("demo",15))
#print(gov_unhappy("demo",16))
#print(gov_unhappy("demo",30))
#print(gov_unhappy("demo",31))
#print(gov_unhappy("demo",32))
#print(gov_unhappy("demo",46))
#print(gov_unhappy("demo",47))
#print(gov_unhappy("demo",48))

print(    """ ** Function mood **
    ""  size        - city size
    ""  foreign_unh - number of citizens due to be made unhappy because foreign 
    ""  ncities     - total number of cities
    ""  buildings_content   - number made content by buildings
    ""  wonders_happy       - number made happy by wonders
    ""  wonders_content     - number made content by wonders
    ""  gov         - one of "demo", "rep", "mon"
    """)

def mood(size,specialists,foreign_unh,ncities,lux,buildings_content,wonders_happy,wonders_content,gov="demo",debug=False):
    """
    ""  size        - city size
    ""  foreign_unh - number of citizens due to be made unhappy because foreign 
    ""  ncities     - total number of cities
    ""  buildings_content   - number made content by buildings
    ""  wonders_happy       - number made happy by wonders
    ""  wonders_content     - number made content by wonders
    ""  gov         - one of "demo", "rep", "mon"
    """
    citizens={"unh":0,"con":0,"hap":0,"spe":specialists}
    #cities
    citizens["con"]=4-gov_unhappy(gov,ncities)
    citizens["unh"]=size-citizens["con"]
    for s in range(0,specialists):
        if (citizens["con"]>0): citizens["con"]-=1;
        else: citizens["unh"]-=1;
    if debug: print ("  cities ",citizens)
    #luxury
    for l in range(0,math.floor(lux/2)):
        if (citizens["con"]>0): citizens["con"]-=1; citizens["hap"]+=1;
        elif (citizens["unh"]>0): citizens["unh"]-=1; citizens["con"]+=1;
    if debug: print ("  luxury ",citizens)
    #buildings
    citizens["con"]+=min(citizens["unh"],buildings_content)
    citizens["unh"]=max(citizens["unh"]-buildings_content,0)
    if debug: print ("  buildings ",citizens)
    #nationality
    for f in range(0,foreign_unh):
        if (citizens["con"]>0): citizens["con"]-=1; citizens["unh"]+=1;
        elif (citizens["hap"]>0): citizens["hap"]-=1; citizens["con"]+=1;
    if debug: print ("  nationality ",citizens)
    #units
    # NOT IMPEMENTED
    #wonders
    for w in range(0,wonders_content):
        if (citizens["unh"]>0): citizens["unh"]-=1; citizens["con"]+=1;
    for w in range(0,wonders_happy):
        if (citizens["con"]>0): citizens["con"]-=1; citizens["hap"]+=1;
        elif (citizens["unh"]>0): citizens["unh"]-=1; citizens["con"]+=1;
    if debug: print ("  wonders ",citizens)
    return citizens

def state(mood,size,opt=""):
    order=(mood["hap"]>=mood["unh"])
    rap=(mood["hap"]>=size/2) and (mood["unh"]==0)
    if opt=="order" or opt=="disorder": return order
    elif opt=="rap": return rap
    elif not order: return "disorder"
    elif rap: return "rap"
    else: return "order"

def elves_to_improve_foreign(size, specialists_from, foreign_unh_seq,ncities,
        trade_per_tile, marco_polo, lux_pct, buildings_lux,
        buildings_content,wonders_happy,wonders_content,gov="demo",debug=False):
    res=[]
    for foreign_unh in foreign_unh_seq:
        workers=size-max(0,size-specialists_from)
        lux=math.floor(math.floor((workers+1)*trade_per_tile
            *(1+.4*marco_polo)*lux_pct/100)*(1+buildings_lux))
        m0=mood(size,size-workers,foreign_unh,ncities,lux,
                buildings_content,wonders_happy,wonders_content,gov="demo",debug=debug)
        if debug: print ("City size ", size,
            " trade ", math.floor((workers+1)*trade_per_tile*(1+.4*marco_polo)),
            " mood ",  m0)
        state0=state(m0,size)
        if state0=="disorder": aim="order"
        elif state0=="order": aim="rap"
        else: 
            if debug: print("...rapture already")
            res+=[(list(m0.values()),[0,0])]
            continue
        m=m0
        elves=0
        elves_vec=[0,0]
        while not state(m,size,aim):
            elves=elves+1
            if debug: print ("Elves ",elves)
            if elves>size-workers: 
                workers-=1
            if workers<0: 
                elves=-1
                if debug: print ("...problem: ",m)
                break
            lux=lux+2*(1+buildings_lux)
            m=mood(size,size-workers,foreign_unh,ncities,lux,
                buildings_content,wonders_happy,wonders_content,gov="demo",debug=debug)
            if aim=="order" and state(m,size,aim):
                elves_vec[0]=elves
                aim="rap"
        elves_vec[1]=elves
        if debug: print ("...found: ",m,elves_vec)
        res+=[(list(m.values()),elves_vec)]
    return res


def elves_to_improve(sizes, specialists_from, foreign_unh,ncities,
        trade_per_tile, marco_polo, lux_pct, buildings_lux,
        buildings_content,wonders_happy,wonders_content,gov="demo",debug=False):
    res=[]
    for s in sizes:
        workers=s-max(0,s-specialists_from)
        lux=math.floor(math.floor((workers+1)*trade_per_tile
            *(1+.4*marco_polo)*lux_pct/100)*(1+buildings_lux))
        m0=mood(s,s-workers,foreign_unh,ncities,lux,
                buildings_content,wonders_happy,wonders_content,gov="demo",debug=debug)
        if debug: print ("City size ", s,
            " trade ", math.floor((workers+1)*trade_per_tile*(1+.4*marco_polo)),
            " mood ",  m0)
        state0=state(m0,s)
        if state0=="disorder": aim="order"
        elif state0=="order": aim="rap"
        else: 
            if debug: print("...rapture already")
            res+=[(list(m0.values()),[0,0])]
            continue
        m=m0
        elves=0
        elves_vec=[0,0]
        while not state(m,s,aim):
            elves=elves+1
            if debug: print ("Elves ",elves)
            if elves>s-workers: 
                workers-=1
            if workers<0: 
                elves=-1
                if debug: print ("...problem: ",m)
                break
            lux=lux+2*(1+buildings_lux)
            m=mood(s,s-workers,foreign_unh,ncities,lux,
                buildings_content,wonders_happy,wonders_content,gov="demo",debug=debug)
            if aim=="order" and state(m,s,aim):
                elves_vec[0]=elves
                aim="rap"
        elves_vec[1]=elves
        if debug: print ("...found: ",m,elves_vec)
        res+=[(list(m.values()),elves_vec)]
    return res

def dood(sizes, specialists_from, foreign_unh,ncities,
        trade_per_tile, marco_polo, lux_pct, buildings_lux,
        buildings_content,wonders_happy,wonders_content,gov="demo",debug=False):
    res=[]
    for s in sizes:
        workers=s-max(0,s-specialists_from)
        lux=math.floor(math.floor((workers+1)*trade_per_tile
            *(1+.4*marco_polo)*lux_pct/100)*(1+buildings_lux))
        print("City ", workers, s-workers, lux)
        m=mood(s,s-workers,foreign_unh,ncities,lux,
            buildings_content,wonders_happy,wonders_content,gov="demo",debug=debug)
        res+=[list(m.values())]
    return res


#def mood(size,specialists,foreign_unh,ncities,lux,buildings_content,wonders_happy,wonders_content,gov="demo"):

#print(mood(size=15,specialists=1,
#    foreign_unh=0,ncities=37,lux=27,buildings_content=2+4,
#    wonders_happy=2,wonders_content=2))

#print(dood([8,12,16,20],18,0,ncities=36,trade_per_tile=2.22,
#    marco_polo=1,lux_pct=30,buildings_lux=1.5,buildings_content=4,
#    wonders_happy=2,wonders_content=0,debug=True))
#
#print(elves_to_improve([8,12,16,20],18,0,ncities=36,trade_per_tile=2.25,
#    marco_polo=1,lux_pct=30,buildings_lux=1.5,buildings_content=4,
#    wonders_happy=2,wonders_content=0,debug=True))

#print ([(i,unhappy_foreigners(i,18, 34,1000,34)) for i in range(12)])

def plot_histo_static(sizes, specialists_from, foreign_unh,ncities,
        trade_per_tile, marco_polo, lux_pct, buildings_lux,
        buildings_content,wonders_happy,wonders_content,gov="demo"):
    ax1 = plt.figure()
    data=elves_to_improve(sizes, specialists_from, foreign_unh,ncities,
        trade_per_tile, marco_polo, lux_pct, buildings_lux,
        buildings_content,wonders_happy,wonders_content,gov)
    for_order=[e[1][0] for e in data] 
    for_rap=[e[1][1] for e in data] 
    plt.bar(sizes,for_rap,width=0.8)
    plt.bar(sizes,for_order,width=0.5)
    plt.ylabel("Number of elves for order/rapture")
    plt.xlabel("City size")
    plt.show()

def plot_histo_interactive(sizes, specialists_from, foreign_unh,ncities,
        trade_per_tile, marco_polo_bool, lux_pct, buildings_lux_string,
        buildings_content,wonders_happy,wonders_content,gov="demo"):
    dic_buildings_lux={'None':0,'Market':.5,'Bank':1,'StockEx':1.5}
    plot_histo_static(sizes, specialists_from, foreign_unh,ncities,
        trade_per_tile, marco_polo_bool*1, lux_pct, 
        dic_buildings_lux[buildings_lux_string],
        buildings_content,wonders_happy,wonders_content,gov)

def foreigners(turns, size, conquest_pct, convert_speed, effect_angry_conquered):
    foreign=math.ceil(size*(100-conquest_pct)/100)
    unhappy=[(foreign,int(np.floor(foreign*effect_angry_conquered/100)))]
    for t in range(1,turns):
        if (foreign>0): foreign-=np.floor(convert_speed/1000*t)/t
        unhappy+=[(foreign,int(np.floor(foreign*effect_angry_conquered/100)))]
    return unhappy

def plot_histo_foreign_interactive(turns, size, specialists_from, 
        conquest_pct, convert_speed, effect_angry_conquered,
        ncities, trade_per_tile, marco_polo_bool, lux_pct, buildings_lux_string,
        buildings_content,wonders_happy,wonders_content,gov="demo"):
    ax2 = plt.figure()
    foreign_seq=foreigners(turns, size, conquest_pct, convert_speed,
        effect_angry_conquered)
    foreign_num_seq=[f[0] for f in foreign_seq]
    foreign_unh_seq=[f[1] for f in foreign_seq]
    dic_buildings_lux={'None':0,'Market':.5,'Bank':1,'StockEx':1.5}
    data=elves_to_improve_foreign(size, specialists_from, foreign_unh_seq, ncities,
        trade_per_tile, marco_polo_bool*1, lux_pct, 
        dic_buildings_lux[buildings_lux_string],
        buildings_content,wonders_happy,wonders_content,gov)
    for_order=[e[1][0] for e in data] 
    for_rap=[e[1][1] for e in data] 
    plt.bar(range(turns),foreign_num_seq,width=0.8,color='k')
    plt.bar(range(turns),for_rap,width=0.6)
    plt.bar(range(turns),for_order,width=0.4)
    plt.ylabel("Number foreign citizens, of elves for order/rapture")
    plt.xlabel("Turns after conquest")
    plt.show()

#plot_histo(range(1,20),16,0,10,2,1,0,0.5,4,2,0)
#print(elves_to_improve([11],16,0,10,2,1,0,0.5,4,2,0,debug=True))
