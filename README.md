# happy civ project

Trying to tune happiness settings for freecivweb.

Target number one: make conquered cities reasonably unhappy to prevent democracy world conquest.

- Interested in freeciv? Check [freecivweb.org](https://freecivweb.org)

- Want to see and play with our current happy results online ? [use binder online with our python code](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2FJocelyn%2Fhappycivproject/master?filepath=happycalulator.ipynb)